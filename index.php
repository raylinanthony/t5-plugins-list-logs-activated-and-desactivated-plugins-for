<?php # -*- coding: utf-8 -*-
/**
 * Plugin Name: T5 Plugin Log
 * Description: Log all plugin (de)activations.
 * Version:     2012.05.27
 * Author:      Thomas Scholz <info@toscho.de>
 * Author URI:  http://toscho.de
 * Colaborator: Raylin Aquino <raylinaquino@gmail.com>
 * License:     MIT
 * License URI: http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright (c) 2012 Thomas Scholz
 */


date_default_timezone_set('America/Santo_Domingo'); 

add_action( 'activated_plugin', 't5_plugin_logger', 10, 2 );
add_action( 'deactivated_plugin', 't5_plugin_logger', 10, 2 );

register_deactivation_hook( __FILE__, 't5_deactivate_plugin_log' );

/**
 * Log plugin activations and deactivations.
 *
 * @param  string $plugin
 * @param  bool   $network_wide
 * @return void
 */
function t5_plugin_logger( $plugin, $network_wide )
{
	$log_size = 10;
	$log      = get_option( 't5_plugin_log' );
	$count_new_log  = (get_option( 't5_count_news_log')) ? get_option( 't5_count_news_log'): 0;

	! $log && $log = array ();

	// Remove the oldest entry.
	//sizeof( $log ) > $log_size and array_shift( $log );

	$plugin_data = get_plugin_data( WP_PLUGIN_DIR . '/' . $plugin );
	$date_format = get_option( 'date_format' ) . ' · ' . get_option( 'time_format' );

	$log[] = array (
		'user'    => esc_html( wp_get_current_user()->display_name ),
		'avatar'  => get_avatar(wp_get_current_user()->ID,32),
		'plugin'  => $plugin_data['Name'],
		'network' => $network_wide ? '✔' : '',
		'time'    => date( $date_format, time() ),
		'action'  => 'deactivated_plugin' === current_filter() ? 'desactivated' : 'activated'
	);
	update_option( 't5_count_news_log', ++$count_new_log );
	update_option( 't5_plugin_log', $log );
}

/**
 * Print the dashboard widget.
 *
 * @return void
 */

function t5_display_plugin_log_dbw()
{
	$log = get_option( 't5_plugin_log' );
	delete_option( 't5_count_news_log' );//Reseting count news updates
?>
<!--T5 DASHBOARD Start-->
<section class="t5-dashboard">
<h1>T5 Plugins Log</h1>
<p><?php _e("Muestra un listado de LOG de los plugins que se activan / desactivan"); ?></p>

	<table id="t5-table-plugins-log" class="widefat">
	<thead>
		<tr>
			<th><?php _e("Usuario"); ?></th>
			<th><?php _e("Plugin");?></th>
			<th><?php _e("Acción");?></th>
			<th><?php _e("Fecha");?></th>
		</tr>
	</thead>
	<tbody>
<?php		

	foreach ( $log as $entry )
	{
		$_action = ($entry["action"] == "activated") ? "active" : "desactive";//Class for style Des/Act
	?>
		<tr>
			<td>
				<div class="t5-user">
					<ul>
						<li><?php echo $entry["avatar"];?></li>
						<li><?php echo $entry["user"];?></li>
					</ul>	
				</div>
			</td>
			<td><h3><?php echo $entry["plugin"]; ?></h3></td>
			<td class="action <?php echo $_action; ?>"><?php echo $entry["action"]; ?></td>
			<td><?php echo $entry["time"]; ?></td>
		</tr>
	<?php
	}
?>


	</tbody></table>
	<?php 

	/*Loads Resources Files Styles and Scripting */
	
	wp_enqueue_style("t5-css", plugins_url("/style.css",__FILE__), '', '', false );
	wp_enqueue_style("t5-datatable-css", "//cdn.datatables.net/1.10.7/css/jquery.dataTables.css", '', '', false );
	wp_enqueue_script("t5-datatable-js", "//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js", '', '', false );
	wp_enqueue_script("t5-js", plugins_url("/main.js",__FILE__), '', '', false );
	

	?>
</section>
<!--end T5 DASHBOARD Start-->
<?php
}


/*
 * Adding Bubble Notifications
 * */

add_filter( 'add_menu_classes', 't5_bubble');

function t5_bubble( $menu ) 
{
    $pending_count = get_option( 't5_count_news_log');//Get Option from T5 Plugins news updates 
   
    foreach( $menu as $menu_key => $menu_data ) 
    {
    	
        if( 't5-plugins-log' == $menu_data[2] and $pending_count){
        
            
        $menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>" . number_format_i18n($pending_count) . '</span></span>';
        }
    }

    return $menu;
}


/**
 * Create a Admin Panel and Menú Panel
 * */

add_action( 'admin_menu', 't5_menu_page' );

function t5_menu_page() {

	add_menu_page( 'T5 Plugin Logs', 'Plugins Logs', 'manage_options', 't5-plugins-log', 't5_display_plugin_log_dbw', 
"dashicons-grid-view", 6 );

}

/**
 * Remove the option for our log.
 *
 * @return void
 */
function t5_deactivate_plugin_log()
{
	// Otherwise the next line wouldn't work.
	remove_action( 'deactivated_plugin', 't5_plugin_logger' );
	// Clean up the options table.
	delete_option( 't5_plugin_log' );
}
?>